﻿_CV

DENYS OSTROVSKYI 

birth_date: 18.07.1992 


- contact_phone_number(++Telegram): +380(93)038-65-87 
- _mail: vedamir.infinum@gmail.com 
- _SkypeID: vedamir-infinum 
- LinkedIn_account: https://www.linkedin.com/in/denys-ostrovskyi-818008126/ 



Motive: 

I am interested in creation of something really cool. Maybe it should be a great game, exatly as we know them now. Maybe it'll be simulated world for posthuman consciousness to live there forever... 
And I can understand now, such projects may be done only in collaboration with high-skilled and motivated people. 



Major skills:

- 3D animation
- Rigging & skinning
- 3D modeling
- Unity 3D
- Unity particles
- Basic scripting\coding (no specific language)

My skills may be demonstrated by files in this repository.



Work experience:

- March 2017 - present: 3D Animator at Heart Beat Games studio.
- 2013 - 2016 self-employed. It means that I was trying myself in several projects, from cheap 3D "printing" company creation to establishing MOOC platform about drugs and medicine. Unfortunatelly, we hasn't required resources and expertise to make this projects happen.



Education: 

2009-2013, National Aerospace University -'Kharkiv Aviation Institute', Bachelor, "Сomputer science". 



Hobbies/Outside interests: 

Above all i'd placed science. Many of the modern scientific discoveries and technological breakthroughs are inspiring me so much. 
To be less abstract: I find biology, genetics and biotechnology to be very interesting. I like thinking about myself in the body of some Zerg-like creaature :) Maybe Overmind woud fit well. 
Next - space is opening for us again, with SpaceX's strategy about cheap orbiting. I am trying not to miss every SpaceX launch and their Falcon 9 first stage landings. 
Games... is this really must be spoken? :) Of course I play games every day. I can't stop on one specific "best game", seriously - I like to try new things coming.