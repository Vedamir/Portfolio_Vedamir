scn zzzAmpQuestScript

float fQuestDelayTime
ref ptr	; PoinTeR(currently absolute pointing on player)
ref DakIdleanim	;Specialidle ref
short init	;init flag
short stage	;not used yet
short PCSpeed	;not used yet
short speedMod	;not used yet
short runningWeaponInit  ; memory for running\walking with weapon speed switch
short weaponState ;not used yet
short fMoveCharWalkMaxGET  ;not used yet
ref EquippedFootsSlot  ;for amp "renewal" check (if boots equip has changed)
short PCSwimState
short idleoverride_flag

Begin GameMode
	if init == 0	; firstRun
		set fQuestDelayTime to 0.2
		set ptr to Player
		let DakIdleanim := AMPPlayerIdleDAK
		set stage to GetStage zzzAMPQuest001
		set PCSwimState to 0
		set init to 1
	endif
	
	if GetGameLoaded == 1 ;Check for load game to reinit non-stored data in save
		let init:= 1
	endif
	
	if zzzAMPPlayerDAK == 1 ;Check if player is DAKAmputee(stored in save)

		if init == 1	;Main initialization(anims enable, game settings adjust)
			let init := call zzzAMPDAKinit ptr
			set EquippedFootsSlot to ptr.GetEquippedObject 5
		endif
		
		if ptr.IsWeaponOut == 1 ;  block player movement if is not running
			if ptr.IsRunning == 1 && runningWeaponInit == 2 ;if running with weapon = unblock movement
				ptr.ModPCMovementSpeed 500
				let runningWeaponInit := 1 ;check for single execution of this condition
			elseif ptr.IsRunning != 1 && runningWeaponInit != 2 ;if trying to walk with weapon = block 
				ptr.ModPCMovementSpeed -500
				let runningWeaponInit := 2 ;check for single execution of this condition
			endif
		;	if ptr.GetWeaponAnimType == 1 && WeaponState != 1	;onehanded
		;		let WeaponState := 1
			;endif
		endif

		if ptr.IsCasting == 1
			ptr.ModPCMovementSpeed -500
		endif
		
		if ptr.IsWeaponOut == 0 	;weapon off
		;	let WeaponState := 0
			if runningWeaponInit == 2 ;if player unequip weapon + trying to walk *not RUN* in the same time(holding walk key/automove on) = unblock movement
				ptr.ModPCMovementSpeed 500
				Let runningWeaponInit := 1 ;check for single execution of this condition
			endif
		endif
		
		if (ptr.GetEquippedObject 5) != (EquippedFootsSlot)	; amp "renewal" check (if boots equip has changed, needed for swim anims "assimilation" via specialidle)
			ptr.playidle DakIdleanim 1
			set EquippedFootsSlot to ptr.GetEquippedObject 5
		endif
	
		if (ptr.IsOnGround == 1) && (PCSwimState == 1) ;shored
			set PCSwimState to 0	;check for single execution of this condition
		endif
		
		if (ptr.IsSwimming == 1) && (PCSwimState != 1)  ;entered water, "assimilating" swim anims via specialidle
			set PCSwimState to 1	;check for single execution of this condition
			ptr.playidle DakIdleanim 1
			set EquippedFootsSlot to ptr.GetEquippedObject 5
		endif
		
	Endif

end