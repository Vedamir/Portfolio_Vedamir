This is my main modding project. Target game - TES IV Oblivion.

It is planned as the realistic and hardcore gameplay modification implementing disabilities for the player character.
This mod alters vanilla death and reload to the "injury" event, that brings player to some safe zone and enables "disability" gameplay mode.
As now, there is only one variant of disability(double leg loss), because it(and every other possible, as well) requires a lot of work on animations to adequately and universally visualize this gameplay.
Even now it is not completely finished.

This mod is composed of several types of work, such as:
- animating:
	-- base stack
	-- wheelchair stack
- scripting (specific internal script language)
- modelling:
	-- Wheelchair for restore some movement ability for disabled player
	-- artificial legs (planned as an artifact made by dwemer race of TES world)

Animations were the major work here. At this point I've handled over 150 anims for "double leg amputation" gameplay mode.
Some of them are edited vanilla anims, other - fully made from scratch.